package hu.wrd.shapes;

public class Rectangle implements Shape {

    private double width = 0;
    private double height = 0;

    Rectangle(double x,double y) throws IllegalArgumentException{
        if (x>0 && y>0) {
            this.width = x;
            this.height = y;
        }
        else{
            throw new IllegalArgumentException("Invalid input! Both x and y must be a positive number!");
        }
    }

    @Override
    public double getArea() {
        return width*height;
    }
    
    public boolean isSquareLike(){
        if (Math.abs(width-height)<0.0000001) {
            return true;
        }
        return false;
    }

    @Override
    public String toString(){
        return "Rectangle with area: " + getArea();
    }
}
