package hu.wrd.shapes;

public class Square implements Shape {

    double width = 0;
    Square(double x) throws IllegalArgumentException{
        if (x>0) {
            width = x;
        }else{
            throw new IllegalArgumentException("Invalid input! x must be a positive number!");
        }
    }
    @Override
    public double getArea() {
        return width*width;
    }
    

    @Override
    public String toString(){
        return "Square with area: " + getArea();
    }
}
