package hu.wrd.shapes;

public class Circle implements Shape {
    private double radius = 0;

    private static double pi = 3.14;

    Circle(double radius) throws IllegalArgumentException {
        if (radius>0) {
            this.radius = radius;
        }else{
            throw new IllegalArgumentException("Invalid input! Radus must be a positive number!");
        }
    }

    @Override
    public double getArea() {
        return radius*radius*pi;
    }

    @Override
    public String toString(){
        return "Circle with area: " + getArea();
    }

}
