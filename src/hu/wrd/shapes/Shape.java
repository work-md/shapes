package hu.wrd.shapes;

public interface Shape extends Comparable<Shape> {
    
    double getArea();

    @Override
    default int compareTo(Shape o) {
        double diff = getArea()-o.getArea();
        if (diff>0) {
            return 1;
        } else if (diff<0) {
            return -1;
        }
        return 0;
    }
}
