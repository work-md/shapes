package hu.wrd.shapes;

import java.util.ArrayList;
import java.util.Collections;

public class ShapeService {
    private ArrayList<Shape> shapes = new ArrayList<>();
    private static double epsilon = 0.0000001;

    public void addShapes(Shape ... shapes) {

        for (int i = 0; i < shapes.length; i++) {
            Shape shape1 = shapes[i];
            for (int j = 0; j < this.shapes.size(); j++) {
                Shape shape2 = this.shapes.get(j);
                    if (isSame(shape1, shape2)) {
                        System.err.println("Shapes not added! List of shapes must contain unique looking shapes!");
                        return;
                    }
            }
        }
        System.out.println("Shapes added successfully");
        for (int i = 0; i < shapes.length; i++) {
            this.shapes.add(shapes[i]);
        }
    }

    public void printShapesOrderByAreaAsc() {
        Collections.sort(shapes);
        printShapes();
    }

    public void printShapesOrderByAreaDesc() {
        Collections.sort(shapes,Collections.reverseOrder());
        printShapes();
    }
    
    private boolean isSame(Shape left,Shape right){
        if (left==right) {
            System.out.println("same! left: " + left + " rigth: " + right );
            return true;
        }else if (Math.abs(left.getArea()-right.getArea())<epsilon) {
            if ( left.getClass()==right.getClass()){
                return true;
            }else if (left.getClass()== Square.class &&  right.getClass()== Rectangle.class) {
                Rectangle tmp = (Rectangle) right;
                if (tmp.isSquareLike()) {
                    return true;
                }
            }else if (left.getClass()== Rectangle.class &&  right.getClass()== Square.class) {
                Rectangle tmp = (Rectangle) left;
                if (tmp.isSquareLike()) {
                    return true;
                }
                return true;
            }
        }
       return false; 
    }
    private void printShapes(){
        for (Shape shape: shapes) {
            System.out.println(shape);
        }
    }
}
